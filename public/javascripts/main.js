/* global $ */

$(function () {
  console.log('main.js loaded.');
  var currentReport = $('#name').val();

  /**
   * Handler for for submission. Sends the form details to
   * the backend and re-renders the page with the appropriate
   * work items.
   */
  $('#name-form').submit(function (event) {
    event.preventDefault();
    console.log('Form submit handler fired.');

    // Clear error message if there is one.
    $('#name-error').text('');

    // Get name user wants to search on.
    var name = $('#name').val();

    // Validate input poorly.
    if (!name || name === '') {
      $('#name-error').text('You need to specify someone.');
      return;
    } else {
      // Disable button so user isn't tempted to try again.
      $('#wrangle-button').prop('disabled', true);

      $.get('/vso?user=' + name, function (res) {
        if (!res.error) {
          $('#content').html(res);
        } else {
          console.error('Error getting bugs. Check server logs.');

          // Re-enable button so user can try again, but pop error message.
          $('#wrangle-button').prop('disabled', false);
          $('#name-error').text('Make sure your two-factor authentication went through. Go to VSO, sign in, and then log out and reauthorize Bug Wrangler.');
        }
      });
    }
  });

  /**
   * Takes the report HTML and emails it to the logged in user.
   */
  $('#send-email').click(function (event) {
    event.preventDefault();
    console.log('\'Email report\' handler fired.');

    $('#send-email').prop('disabled', true);

    // Get the HTML of the report.
    var email = $('#bug-report').html();

    // Sends the HTML to the service that fires off an email to the signed in user.
    $.post('/email-report', { 'email': email }, function (res) {
      $('#send-email').prop('disabled', false);

      if (res === 'OK') {
        console.info('Sent the report in an email to the user.');
      } else {
        console.error('Error, error! Failed to send mail.');
      }
    });
  });

  /**
   * Refreshes the current report by executing the API request
   * with the name of the user the report is currently displayed for.
   */
  $('#refresh').click(function (event) {
    event.preventDefault();
    console.log('\'Refresh\' handler fired.');

    // Disable button so user isn't tempted to try again.
    $('#refresh').prop('disabled', true);

    $.get('/vso?user=' + currentReport, function (res) {
      if (!res.error) {
        console.info('Refreshed bug report.');
        $('#content').html(res);
      } else {
        console.error(res.error);
        console.error('Error getting bugs. Check server logs.');

        // Re-enable button so user can try again, but pop error message.
        $('#refresh').prop('disabled', false);
      }
    });
  });
});
