module.exports = function (passport) {
  var express = require('express');
  var router = express.Router();

  // GET /auth/vso
  //   Use passport.authenticate() as route middleware to authenticate the
  //   request.  The first step in Visual Studio Online authentication will involve
  //   redirecting the user to live.com.  After authorization, Visual Studio Online
  //   will redirect the user back to this application at
  //   /auth/vso/callback
  router.get('/vso',
    passport.authenticate('vso'),
    function (req, res) {
      // The request will be redirected to Visual Studio Online for authentication, so
      // this function will not be called.
    });

  // GET /auth/vso/callback
  //   Use passport.authenticate() as route middleware to authenticate the
  //   request.  If authentication fails, the user will be redirected back to the
  //   login page.  Otherwise, the primary route function function will be called,
  //   which, in this example, will redirect the user to the home page.
  router.get('/vso/callback',
    passport.authenticate('vso', { failureRedirect: '/' }),
    function (req, res) {
      res.redirect('/');
    });

  // GET /auth/vso/logOut
  //    Use the .logOut function exposed by Passport to clear the user from
  //    the session and redirect back to the homepage.
  router.get('/vso/logOut', function (req, res) {
    req.logOut();
    res.redirect('/');
  });

  return router;
};
