var express = require('express');
var nodemailer = require('nodemailer');
var router = express.Router();

router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Bug Wrangler',
    user: req.user,
    formData: {
      name: ''
    },
    assignedToBugs: null,
    createdByBugs: null
  });
});

router.post('/email-report', function (req, res, next) {
  var transporter = nodemailer.createTransport({
    service: 'mailgun',
    auth: {
      user: 'postmaster@sandboxdca1b40419444b60bdc9fbae7f3caf28.mailgun.org',
      pass: '8ec830e14aed8ea73458e01a5c49d19e'
    }
  });

  var mailOptions = {
    from: 'Bug Wrangler <postmaster@sandboxdca1b40419444b60bdc9fbae7f3caf28.mailgun.org>',
    to: req.user._json.emailAddress,
    subject: 'Bug Wrangler report',
    html: req.body.email
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      res.sendStatus(400);
    } else {
      res.sendStatus(200);
    }
  });
});

module.exports = router;
