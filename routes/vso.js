var express = require('express');
var request = require('request');
var Promise = require('promise');
var vsoApp = require('../config/vsoAppInfo');
var router = express.Router();

// GET /vso
//   Takes a query parameter of the user's name. Probably
//   should validate that here (TODO). Returns an object with
//   arrays of bugs assigned to and created by the query param
//   user.
router.get('/', isTokenStale, function (req, res, next) {
  // Get name of user and token from request.
  var user = req.query.user;
  var accessToken = req.user.accessToken;

  // Get bugs assigned to and created by user, then return.
  Promise.all([getBugsAssignedToUser(user, accessToken), getBugsCreatedByUser(user, accessToken)])
    .then(function (results) {
      var assignedTo = [];
      var createdBy = [];

      if (results[0] !== 'none') {
        assignedTo = JSON.parse(results[0].body).value.map(assignContextualClass);
      }

      if (results[1] !== 'none') {
        createdBy = JSON.parse(results[1].body).value.map(assignContextualClass);
      }

      res.render('index', {
        title: 'Bug Wrangler',
        user: req.user,
        formData: {
          name: user
        },
        assignedToBugs: assignedTo,
        createdByBugs: createdBy
      });
    }, function (errors) {
      res.send({
        'error': true
      });
    });
});

// @name getBugsAssignedToUser
// @desc Searches VSO for bugs assigned to given user.
// @param user The name of the target user.
// @param accessToken The token used to authenticate the API call.
// @returns A promise that results in an array of bugs.
function getBugsAssignedToUser (user, accessToken) {
  var promise = new Promise(function (resolve, reject) {
    // Build the API call to VSO API to get bugs assigned to user.
    var apiCall = {
      method: 'POST',
      uri: 'https://office.visualstudio.com/DefaultCollection/_apis/wit/wiql?api-version=1.0',
      headers: {
        'Authorization': 'Bearer ' + accessToken,
        'Content-Type': 'application/json'
      },
      json: {
        'query': "SELECT [System.Id] FROM WorkItems WHERE [System.WorkItemType] = 'Bug' AND [State] <> 'Closed' AND [System.AssignedTo] = '" + user + "'"
      }
    };

    // Make the API call to get bugs assigned to user.
    request(apiCall, function (error, response) {
      if (error) {
        reject(error);
      }

      if (response.statusCode === 200) {
        var bugs = response.body.workItems;
        var bugIds = [];

        // Return 'none' so an error isn't returned in the next call.
        if (bugs.length === 0) {
          resolve('none');
        }

        // Get bug IDs.
        for (var i = 0; i < bugs.length; i++) {
          bugIds.push(bugs[i].id);
        }

        // Build the API call to get info about bugs assigned to user.
        var infoApiCall = {
          method: 'GET',
          uri: 'https://office.visualstudio.com/DefaultCollection/_apis/wit/WorkItems?ids=' + bugIds.toString() + '&fields=System.Id,System.Title,System.State&api-version=1.0',
          headers: {
            'Authorization': 'Bearer ' + accessToken,
            'Content-Type': 'application/json'
          }
        };

        // Make the API call to get info about bugs assigned to user.
        request(infoApiCall, function (error, response) {
          if (error) {
            reject(error);
          }

          if (response.statusCode === 200) {
            resolve(response);
          } else {
            reject(response.statusCode);
          }
        });
      } else {
        console.log(response);
        reject(response.statusCode);
      }
    });
  });

  return promise;
}

// @name getBugsCreatedByUser
// @desc Searches VSO for bugs created by given user.
// @param user The name of the target user.
// @param accessToken The token used to authenticate the API call.
// @returns A promise that results in an array of bugs.
function getBugsCreatedByUser (user, accessToken) {
  var promise = new Promise(function (resolve, reject) {
    // Build the API call to VSO API to get bugs created by user.
    var apiCall = {
      method: 'POST',
      uri: 'https://office.visualstudio.com/DefaultCollection/_apis/wit/wiql?api-version=1.0',
      headers: {
        'Authorization': 'Bearer ' + accessToken,
        'Content-Type': 'application/json'
      },
      json: {
        'query': "SELECT [System.Id] FROM WorkItems WHERE [System.WorkItemType] = 'Bug' AND [State] <> 'Closed' AND [System.CreatedBy] = '" + user + "'"
      }
    };

    // Make the API call to get bugs created by user.
    request(apiCall, function (error, response) {
      if (error) {
        reject(error);
      }

      if (response.statusCode === 200) {
        var bugs = response.body.workItems;
        var bugIds = [];

        // Return 'none' so an error isn't returned in the next call.
        if (bugs.length === 0) {
          resolve('none');
        }

        // Get bug IDs.
        for (var i = 0; i < bugs.length; i++) {
          bugIds.push(bugs[i].id);
        }

        // Build the API call to get info about bugs created by user.
        var infoApiCall = {
          method: 'GET',
          uri: 'https://office.visualstudio.com/DefaultCollection/_apis/wit/WorkItems?ids=' + bugIds.toString() + '&fields=System.Id,System.Title,System.State,System.AssignedTo&api-version=1.0',
          headers: {
            'Authorization': 'Bearer ' + accessToken,
            'Content-Type': 'application/json'
          }
        };

        // Make the API call to get info about bugs created by user.
        request(infoApiCall, function (error, response) {
          if (error) {
            reject(error);
          }

          if (response.statusCode === 200) {
            resolve(response);
          } else {
            reject(response.statusCode);
          }
        });
      } else {
        console.log(response);
        reject(response.statusCode);
      }
    });
  });

  return promise;
}

// @name assignContextualClass
// @desc Use in a map function to assign a CSS class to a bug.
// @param bug A bug (work item) from VSO.
// @returns Returns the bug param, with a contextual CSS class.
function assignContextualClass (bug) {
  switch (bug.fields['System.State']) {
    case 'Active':
      bug.class = 'danger';
      break;
    case 'Resolved':
      bug.class = 'success';
      break;
    case 'New':
      bug.class = 'warning';
      break;
    default:
      break;
  }

  return bug;
}

function isTokenStale (req, res, next) {
  console.log('isTokenStale called.');
  var isStale = Date.now() > req.user.expirationTime;
  
  if (isStale) {
    refreshToken(req, res, next);    
  }
  else {
    next();
  }
}

function refreshToken (req, res, next) {
  console.log('refreshToken called.');
  
  var refreshTokenRequest = {
      method: 'POST',
      uri: 'https://app.vssps.visualstudio.com/oauth2/token',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      form: {
        'client_assertion_type': 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
        'client_assertion': vsoApp.clientSecret,
        'grant_type': 'refresh_token',
        'assertion': req.user.refreshToken,
        'redirect_uri': vsoApp.callbackURL
      }
  };
  
  request(refreshTokenRequest, function (error, response) {
    if (error) {
      console.log('Unable to get a new access token.');
    } else {
      var tokens = JSON.parse(response.body);

      req.user.accessToken = tokens.access_token;
      req.user.refreshToken = tokens.refresh_token;
      req.user.expirationTime = new Date((new Date()).getTime() + 50 * 60000).getTime();
      
      next();
    }
  });
}

module.exports = router;
