var Strategy = require('passport-vso').Strategy;

module.exports = function (passport) {
  // VSO app informaton. Move to a config file eventually.
  var vsoApp = require('./vsoAppInfo');

  // Passport session setup.
  passport.serializeUser(function (user, done) {
    done(null, user);
  });

  passport.deserializeUser(function (obj, done) {
    done(null, obj);
  });

  // Configure VSO Passport strategy.
  passport.use(new Strategy({
    clientID: vsoApp.clientID,
    clientSecret: vsoApp.clientSecret,
    callbackURL: vsoApp.callbackURL,
    scope: 'vso.work',
    state: true
  },
    function (accessToken, refreshToken, params, profile, done) {
      // Asynchronous verification, for effect...
      process.nextTick(function () {
        // To keep the example simple, the user's Visual Studio Online profile is returned
        // to represent the logged-in user.  In a typical application, you would
        // want to associate the Visual Studio Online account with a user record in your
        // database, and return that user instead.
        profile.accessToken = accessToken;
        profile.expirationTime = new Date((new Date()).getTime() + 50 * 60000).getTime();
        profile.refreshToken = refreshToken;
        return done(null, profile);
      });
    }
  ));
};
